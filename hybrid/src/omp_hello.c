#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main(int argc, char *argv[])
{
    int iam = 0, np = 1;
   
    #pragma omp parallel private(iam)
    {
        np = omp_get_num_threads();
        iam = omp_get_thread_num();
        printf("omp: Hello from thread %d out of %d from process %d out of %d\n", iam, np, 0, 1);
    }

    return 0;
}

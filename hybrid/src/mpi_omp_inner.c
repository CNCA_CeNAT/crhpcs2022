
#include <stdio.h>
#include <stdlib.h>

#define N 1000000000
int main(int argc, char* argv[]) {

	double* a_local;
	double* b_local;
	double prod_local;
	long int i;

	a_local = (double*)malloc (N*sizeof(double));
	b_local = (double*)malloc (N*sizeof(double));

	for (i=0; i<N; i++) {
		a_local[i] = (double)i+1;
		b_local[i] = 2*(double)i+1;
	}

	prod_local = 0.0;
	
	for (i=0; i < N; i++) {
		prod_local += a_local[i] * b_local[i];
	}
	printf("Local dotProduct = %le  s\n", prod_local);

	return (0);
}
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char **argv){
    if(argc != 2){
        printf("Usage: %s 10000000000\n", argv[0]);
        exit(1);
    }

    long long int i, n = atoll(argv[1]);
    double pi, h, x, sum = 0.0;

    h = 1.0 / (double) n;
    
    for(i = 1; i <= n; i++){ 
        x = h * ( (double) i - 0.5);
        sum += (4.0 / (1.0 + x * x));
    }
    pi = h * sum;
    printf ("pi: %.6f \n", pi);
}